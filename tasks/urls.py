from django.urls import path
from .views import *
urlpatterns = [
    path( 'new_task/', TaskCreateView.as_view(), name='new_task' ),
    path( 'edit_task/<int:pk>', TaskUpdateView.as_view(), name='edit_task' ),
    path( 'delete_task/<int:pk>', TaskDeleteView.as_view(), name='delete_task' ),
    path( 'signup/', SignUpView.as_view(), name="signup" ),
    path( '', TaskListView.as_view(), name='home' )
]