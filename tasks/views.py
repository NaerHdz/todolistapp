#from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import ListView
from django.urls import reverse_lazy

from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views import generic

from .models import Task
# Create your views here.
class TaskListView ( ListView ):
    model = Task
    template_name = 'home.html'

class TaskCreateView ( CreateView ):
    model = Task
    template_name = 'new_task.html'
    fields = '__all__'

class TaskUpdateView ( UpdateView ):
    model = Task
    template_name = 'edit_task.html'
    fields = ['title','body']

class TaskDeleteView ( DeleteView ):
    model = Task
    template_name = 'delete_task.html'
    success_url = reverse_lazy ( 'home' )

class SignUpView ( generic.CreateView ):
    form_class = UserCreationForm
    success_url = reverse_lazy( 'login' )
    template_name = 'signup.html'